describe("Home E2E", () => {
	it("Home correct", () => {
		cy.login("john@example.com", "2aSsword95%");

		cy.visit("/");
		
		cy.get(".user-name").should("have.text", "John Doe");
		cy.get(".user-rut").should("have.text", "22.222****");
		cy.get(".user-email").should("have.text", "john@example.com");
	});

	it("Home incorrect", () => {
		cy.window().then((win) => {
			win.localStorage.clear();
		});

		cy.visit("/");

		cy.get("div")
			.find(".q-notification")
			.should("be.visible")
			.and("contain.text", "Please enter your credentials");

		cy.on("url:changed", (newurk) => {
			console.log(newurk);
			cy.url().should("eq", `${Cypress.config().baseUrl}/login?login_error=1`);
		});
	});
	it("Correct login with unverified user", () => {
		cy.visit("/login");
		cy.get('input[type="email"]').type("s.cifuentes02@ufromail.cl"); // Utilizar el email de un usuario no verificado
		cy.get('input[type="password"]').type("Password123!");
		cy.get('button[type="submit"]').click();

		cy.get("div")
			.find(".q-notification")
			.should("be.visible")
			.and("contain.text", "You need to verify your account");

		cy.url().should("include", "/verify");
		cy.url().should("include", "verify_error=1");
	});
	it("Get user's RUT and avatar by clicking on the card", () => {
		cy.login("john@example.com", "2aSsword95%");

		cy.visit("/");
		cy.get('div[id="user"]').click();


		cy.get(".q-card")
			.should("be.visible")
			.within(() => {
				cy.get('div[id="user-rut-complete"]').should("have.text", "RUT: 22.222.222-2");
				cy.get(".q-avatar").should("be.visible");
			});
	});
	
	it("Block user by clicking on the block button", () => {
		cy.login("john@example.com", "2aSsword95%");

		cy.visit("/");
		cy.get('button').first().click();

		cy.get(".text-caption").should("have.text", "This user is blocked");
	});

	it("Unblock user by clicking on the unblock button", () => {
		cy.login("john@example.com", "2aSsword95%");

		cy.visit("/");
		cy.get('button').first().click();

		cy.get(".text-caption").should("have.text", "This user is unblocked");
	});
});

describe("Verificación E2E", () => {
    it("Correct verification", () => {
        cy.login("john@example.com", "2aSsword95%").then(() => {//considerar hacer la prueba con un usuario no verificado
            const verificationCode = "asd123"; // Código de verificación correcto

            cy.visit("/verify");

            cy.get('input[name="verify-code"]').type(verificationCode);
            cy.get('button[type="submit"]').click();

            cy.get("div")
                .find(".q-notification")
                .should("be.visible")
                .and("contain.text", "Verification successful");

            cy.url().should("eq", `${Cypress.config().baseUrl}/`);
        });
    });
    it("Incorrect verification due to invalid code", () => {
        cy.login("s.cifuentes02@ufromail.cl", "Password123!").then(() => {
            const verificationCode = "CODIGO_INCORRECTO"; // Reemplaza con un código incorrecto

            cy.visit("/verify");

            cy.get('input[name="verify-code"]').type(verificationCode);
            cy.get('button[type="submit"]').click();

            cy.get("div")
                .find(".q-notification")
                .should("be.visible")
                .and("contain.text", "the code is not valid");
        });
    });
    it("Unnecessary verification", () => {
        cy.login("john@example.com", "2aSsword95%").then(() => {
            const verificationCode = "asd123"; // Reemplaza con el código de verificación correcto

            cy.visit("/verify");

            cy.get('input[name="verify-code"]').type(verificationCode);
            cy.get('button[type="submit"]').click();

            cy.get("div")
                .find(".q-notification")
                .should("be.visible")
                .and("contain.text", "the user is already verified");

            cy.url().should("eq", `${Cypress.config().baseUrl}/`);

        });
    });
});